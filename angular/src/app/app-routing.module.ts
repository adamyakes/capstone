import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DpComponent } from './dp/dp.component';
import { DpStatsComponent } from './dp-stats/dp-stats.component';


const routes: Routes = [
  { path: '', redirectTo: '/dp', pathMatch: 'full'},
  { path: 'dp-stats', component: DpStatsComponent },
  { path: 'dp', component: DpComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
