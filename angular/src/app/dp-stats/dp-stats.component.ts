import { Component, OnInit } from '@angular/core';
import { Run, StatsReq } from '../css_pb'
import { PhilosopherClient } from '../css_grpc_web_pb'

@Component({
  selector: 'app-dp-stats',
  templateUrl: './dp-stats.component.html',
  styleUrls: ['./dp-stats.component.scss']
})
export class DpStatsComponent implements OnInit {
  public runs: Run[]
  philosopherService = new PhilosopherClient('http://localhost:8080')

  constructor() { }

  ngOnInit(): void {
    let thisRef = this
    this.philosopherService.getStats(new StatsReq(), {}, function(err, response) {
      thisRef.runs = response.getRunsList()
      thisRef.runs.forEach(run => console.log(run.getTimestamp(), run.getDeadlocked(), run.getStatsList()))
    })
  }

}
