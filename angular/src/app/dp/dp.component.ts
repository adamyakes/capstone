import { Component, OnInit } from '@angular/core';
import { Options, State } from '../css_pb'
import { PhilosopherClient } from '../css_grpc_web_pb'
import { SVG, Circle, Rect, Text, Line, Timeline } from '@svgdotjs/svg.js'

@Component({
  selector: 'app-dp',
  templateUrl: './dp.component.html',
  styleUrls: ['./dp.component.scss']
})
export class DpComponent implements OnInit {
  states: PictureState[] = []
  interval = 0
  p: Picture
  philosopherService = new PhilosopherClient('http://localhost:8080')

  constructor() { }

  ngOnInit(): void {
    this.initialize()
  }

  getNumPhilosophers() { return Number((<HTMLInputElement>document.getElementById('num-phils')).value) }
  getAnimDur() { return Number((<HTMLInputElement>document.getElementById('anim-dur')).value) }

  makePicture() {
    let numPhilosophers = this.getNumPhilosophers()

    let container = document.getElementById('svg-container')
    while (container.lastChild) container.removeChild(container.lastChild)

    var draw = SVG().addTo('#svg-container').size(350, 350)

    // Table
    draw.circle().radius(100).move(75, 75).attr({ fill: '#fff', stroke: '#000' })
    var philosophers: Circle[] = []
    var philosopherCoords: Coordinate[] = []
    var resources: Rect[] = []
    var resourceCoords: Coordinate[] = []
    var philosopherTexts: Text[] = []
    var lines: Line[][] = []
    let timeline = new Timeline()
    this.p = {
      philosophers,
      philosopherCoords,
      resources,
      resourceCoords,
      philosopherTexts,
      lines,
      timeline,
    }
    for (let i = 0; i < numPhilosophers; i++) {
      let resCoord = this.alongCircle(175, 175, 75, 2 * Math.PI * (2 * i + 1) / (2 * numPhilosophers))
      resourceCoords.push(resCoord)
      resources.push(draw.rect(12.5, 12.5).move(resCoord.x - 6.25, resCoord.y - 6.25).attr({ fill: '#fff', stroke: '#000' }));

      let philCoord = this.alongCircle(175, 175, 125, 2 * Math.PI * (i + 1) / numPhilosophers)
      philosophers.push(draw.circle(25).move(philCoord.x - 12.5, philCoord.y - 12.5).attr({ fill: '#ff0', stroke: '#000' }))
      philosopherCoords.push(philCoord)
      let textCoord = this.alongCircle(175, 175, 150, 2 * Math.PI * (i + 1) / numPhilosophers)
      philosopherTexts.push(draw.text('').move(textCoord.x - 10, textCoord.y - 15))
    }
    // Draw the lines
    for (let i = 0; i < numPhilosophers; i++) {
      let phil = philosopherCoords[i]
      let res = resourceCoords[i]
      let baseX = Math.min(phil.x, res.x)
      let baseY = Math.min(phil.y, res.y)
      let left = draw.line(
        phil.x - baseX,
        phil.y - baseY,
        res.x - baseX,
        res.y - baseY,
      ).move(baseX, baseY).stroke({ color: '#000', width: 2, linecap: 'round' })
      res = resourceCoords[(i + 1) % numPhilosophers]
      baseX = Math.min(phil.x, res.x)
      baseY = Math.min(phil.y, res.y)
      let right = draw.line(
        phil.x - baseX,
        phil.y - baseY,
        res.x - baseX,
        res.y - baseY,
      ).move(baseX, baseY).stroke({ color: '#000', width: 2, linecap: 'round' })

      left.stroke({ opacity: 0 })
      right.stroke({ opacity: 0 })
      lines.push([left, right])
    }

    philosophers.forEach(p => p.timeline(timeline))
    philosopherTexts.forEach(t => t.timeline(timeline))
  }

  playing = false
  playStates() {
    let button = <HTMLButtonElement>document.getElementById('play-button')
    if (!this.playing) {
      let animDur = this.getAnimDur()
      let startAtInput = <HTMLInputElement>document.getElementById('start-at')
      let at = Number(startAtInput.value)
      this.interval = window.setInterval(() => {
        this.setState(at)
        at++
        startAtInput.value = at.toString()
      }, animDur)
      button.innerText = 'Pause'
    } else {
      window.clearInterval(this.interval)
      button.innerText = 'Play'
    }
    this.playing = !this.playing
  }

  forward() {
    let startAtInput = <HTMLInputElement>document.getElementById('start-at')
    let newAt = (Number(startAtInput.value) + 1)
    startAtInput.value = newAt.toString()
    this.setState(newAt)
  }

  back() {
    let startAtInput = <HTMLInputElement>document.getElementById('start-at')
    let newAt = (Number(startAtInput.value) - 1)
    startAtInput.value = newAt.toString()
    this.setState(newAt)
  }

  initialState(p: Picture): PictureState {
    return ({
      philosophers: p.philosophers.map(_ => PhilosopherState.Initial),
      lines: p.lines.map(_ => [LineState.Nothing, LineState.Nothing]),
      resources: p.resources.map(_ => ResourceState.Unused),
    })
  }

  transform(incoming): PictureState {
    let oldState: PictureState = this.states[this.states.length - 1]
    let newState: PictureState = {
      philosophers: oldState.philosophers.map(s => s),
      lines: oldState.lines.map(arr => arr.map(s => s)),
      resources: oldState.resources.map(s => s),
    }
    let incomingState: string = incoming.getState()
    let id: number = incoming.getId()
    if (incomingState == 'Hungry') {
      newState.philosophers[id] = PhilosopherState.Hungry
    } else if (incomingState == 'Eating') {
      newState.philosophers[id] = PhilosopherState.Eating
    } else if (incomingState == 'Thinking') {
      newState.philosophers[id] = PhilosopherState.Thinking
    } else if (incomingState == 'Deadlock detected') {
      newState.philosophers[id] = PhilosopherState.Deadlocked
    } else if (incomingState == 'Gracefully exited') {
      newState.philosophers[id] = PhilosopherState.Exited
    } else if (incomingState.includes('Holding')) {
      let resourceNumber = Number(incomingState.substring(13))
      newState.resources[resourceNumber] = ResourceState.Used
      if (resourceNumber == id) { // left
        newState.lines[id][0] = LineState.Consume
      } else {
        newState.lines[id][1] = LineState.Consume
      }
    } else if (incomingState.includes('Releasing')) {
      let resourceNumber = Number(incomingState.substring(15))
      newState.resources[resourceNumber] = ResourceState.Unused
      if (resourceNumber == id) { // left
        newState.lines[id][0] = LineState.Nothing
      } else {
        newState.lines[id][1] = LineState.Nothing
      }
    } else if (incomingState.includes('Requesting')) {
      let resourceNumber = Number(incomingState.substring(16))
      if (resourceNumber == id) { // left
        newState.lines[id][0] = LineState.Request
      } else {
        newState.lines[id][1] = LineState.Request
      }
    }
    return newState
  }

  alongCircle(centerX: number, centerY: number, radius: number, angle: number): { x: number, y: number } {
    return {
      x: centerX + (Math.cos(angle) * radius),
      y: centerY + (Math.sin(angle) * radius),
    }
  }



  runDP() {
    this.clear()
    const list = document.getElementById('list')
    var request = new Options()
    request.setChoice(Number((<HTMLSelectElement>document.getElementById('solution')).value))
    request.setDuration(1000 * Number((<HTMLInputElement>document.getElementById('duration-input')).value))
    request.setSize(this.getNumPhilosophers())
    request.setSleep(1000 * Number((<HTMLInputElement>document.getElementById('sleep-input')).value))
    request.setLeftiesList(null)

    this.makePicture()
    this.states.push(this.initialState(this.p))
    var stream = this.philosopherService.philosophize(request, {})
    stream.on('data', response => {
      this.states.push(this.transform(response))
      // const node = document.createElement('li')
      // node.textContent = 'Philosopher #' + response.getId() + ' is in state ' + response.getState()
      // list.prepend(node)

      const id = 'ph-' + (1 + response.getId())
      const stateCell = document.getElementById(id)
      stateCell.textContent = response.getState()

      const thinkingCell = stateCell.nextSibling
      if (response.getState() == 'Thinking') {
        thinkingCell.textContent = String(Number(thinkingCell.textContent) + 1)
        return
      }
      const hungryCell = thinkingCell.nextSibling
      if (response.getState() == 'Hungry') {
        hungryCell.textContent = String(Number(hungryCell.textContent) + 1)
      }

      const eatingCell = hungryCell.nextSibling
      if (response.getState() == 'Eating') {
        eatingCell.textContent = String(Number(eatingCell.textContent) + 1)
      }
    })

    stream.on('error', err => console.log('Error! ', err))
    stream.on('end', () => this.refresh())
    stream.on('status', status => console.log('Status: ', status))
  }

  refresh() {

  }

  setLineState(line: Line, state: LineState): void {
    if (state == LineState.Consume) {
      line.stroke({ opacity: 100 })
      let endreference = line.reference('marker-end')
      if (endreference) endreference.remove()
      line.marker('start', 10, 10, function (add) {
        add.polygon('5,5 10,7.5 10,2.5')
      })
    } else if (state == LineState.Request) {
      line.stroke({ opacity: 100 })
      let startreference = line.reference('marker-start')
      if (startreference) startreference.remove()
      line.marker('end', 10, 10, function (add) {
        add.polygon('5,5 0,7.5 0,2.5')
      })
    } else if (state == LineState.Nothing) {
      let endreference = line.reference('marker-end')
      if (endreference) endreference.remove()
      let startreference = line.reference('marker-start')
      if (startreference) startreference.remove()
      line.stroke({ opacity: 0 })
    }
  }

  setPhilosopherState(p: Circle, t: Text, state: PhilosopherState, animDur: number): void {
    let color: string, text: string
    if (state == PhilosopherState.Initial) {
      color = '#fff'
      text = ''
    } else if (state == PhilosopherState.Thinking) {
      color = '#00ff00'
      text = 'T'
    } else if (state == PhilosopherState.Hungry) {
      color = '#0000ff'
      text = 'H'
    } else if (state == PhilosopherState.Eating) {
      color = '#ff0000'
      text = 'E'
    } else if (state == PhilosopherState.Exited) {
      color = '#ffffff'
      text = 'G'
    } else if (state == PhilosopherState.Deadlocked) {
      text = '💀'
      color = '#737373'
    }
    p.animate({ duration: animDur, delay: 0 }).attr({ fill: color })
    t.text(text)
  }

  setResourceState(r: Rect, state: ResourceState, animDur: number): void {
    if (state == ResourceState.Used) {
      r.animate({ duration: animDur, delay: 0, }).attr({ fill: '#f00' })
    } else if (state == ResourceState.Unused) {
      r.animate({ duration: animDur, delay: 0, }).attr({ fill: '#fff' })
    }
  }

  setState(at: number) {
    let animDur = this.getAnimDur()
    let cur = this.states[at]
    cur.lines.forEach((arr, i) => {
      this.setLineState(this.p.lines[i][0], arr[0])
      this.setLineState(this.p.lines[i][1], arr[1])
    })
    cur.philosophers.forEach((philState, i) => this.setPhilosopherState(
      this.p.philosophers[i],
      this.p.philosopherTexts[i],
      philState,
      animDur,
    ))
    cur.resources.forEach((resState, i) => this.setResourceState(this.p.resources[i], resState, animDur))
  }

  clear() {
    (<HTMLInputElement>document.getElementById('start-at')).value = '0'
    let container = document.getElementById('svg-container')
    while (container.lastChild) container.removeChild(container.lastChild)
    const list = document.getElementById('list')
    while (list.lastChild) { list.removeChild(list.lastChild) }
    this.states = []
    const tableHeader = document.getElementById('state-table-header')
    while (tableHeader.nextSibling) { tableHeader.nextSibling.remove() }

    let numPhilosophers = this.getNumPhilosophers()
    for (let i = 0; i < numPhilosophers; i++) {
      const id = 'ph-' + (1 + i)
      let row = document.createElement('tr')
      let temp = document.createElement('td')
      temp.innerText = (i + 1).toString()
      row.appendChild(temp)
      temp = document.createElement('td')
      temp.innerText = 'Initial'
      temp.id = id
      row.appendChild(temp)
      temp = document.createElement('td')
      temp.innerText = '0'
      row.appendChild(temp)
      temp = document.createElement('td')
      temp.innerText = '0'
      row.appendChild(temp)
      temp = document.createElement('td')
      temp.innerText = '0'
      row.appendChild(temp)
      tableHeader.parentNode.appendChild(row)
    }
  }

  initialize() {
    this.clear()
  }

}

interface Coordinate {
  x: number
  y: number
}

interface PictureState {
  philosophers: PhilosopherState[]
  lines: LineState[][]
  resources: ResourceState[]
}

enum PhilosopherState {
  Initial,
  Thinking,
  Hungry,
  Eating,
  Exited,
  Deadlocked,
}

enum LineState {
  Request,
  Consume,
  Nothing,
}

enum ResourceState {
  Used,
  Unused,
}

interface Picture {
  philosophers: Circle[]
  philosopherCoords: Coordinate[]
  resources: Rect[]
  resourceCoords: Coordinate[]
  philosopherTexts: Text[]
  lines: Line[][]
  timeline: Timeline
}
