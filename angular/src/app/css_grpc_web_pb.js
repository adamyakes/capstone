/**
 * @fileoverview gRPC-Web generated client stub for 
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js')
const proto = require('./css_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.PhilosopherClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.PhilosopherPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.Options,
 *   !proto.State>}
 */
const methodDescriptor_Philosopher_Philosophize = new grpc.web.MethodDescriptor(
  '/Philosopher/Philosophize',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.Options,
  proto.State,
  /**
   * @param {!proto.Options} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.State.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.Options,
 *   !proto.State>}
 */
const methodInfo_Philosopher_Philosophize = new grpc.web.AbstractClientBase.MethodInfo(
  proto.State,
  /**
   * @param {!proto.Options} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.State.deserializeBinary
);


/**
 * @param {!proto.Options} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.State>}
 *     The XHR Node Readable Stream
 */
proto.PhilosopherClient.prototype.philosophize =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/Philosopher/Philosophize',
      request,
      metadata || {},
      methodDescriptor_Philosopher_Philosophize);
};


/**
 * @param {!proto.Options} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.State>}
 *     The XHR Node Readable Stream
 */
proto.PhilosopherPromiseClient.prototype.philosophize =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/Philosopher/Philosophize',
      request,
      metadata || {},
      methodDescriptor_Philosopher_Philosophize);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.StatsReq,
 *   !proto.Runs>}
 */
const methodDescriptor_Philosopher_GetStats = new grpc.web.MethodDescriptor(
  '/Philosopher/GetStats',
  grpc.web.MethodType.UNARY,
  proto.StatsReq,
  proto.Runs,
  /**
   * @param {!proto.StatsReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.Runs.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.StatsReq,
 *   !proto.Runs>}
 */
const methodInfo_Philosopher_GetStats = new grpc.web.AbstractClientBase.MethodInfo(
  proto.Runs,
  /**
   * @param {!proto.StatsReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.Runs.deserializeBinary
);


/**
 * @param {!proto.StatsReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.Runs)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.Runs>|undefined}
 *     The XHR Node Readable Stream
 */
proto.PhilosopherClient.prototype.getStats =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/Philosopher/GetStats',
      request,
      metadata || {},
      methodDescriptor_Philosopher_GetStats,
      callback);
};


/**
 * @param {!proto.StatsReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.Runs>}
 *     A native promise that resolves to the response
 */
proto.PhilosopherPromiseClient.prototype.getStats =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/Philosopher/GetStats',
      request,
      metadata || {},
      methodDescriptor_Philosopher_GetStats);
};


module.exports = proto;

