# Concurrency Visualization Tool
This is a forked repository of [Adam Yake's capstone project](https://gitlab.com/adamyakes/capstone) at University of Wisconsin La Crosse. The project aims to visualize common concurrency problems to make them easier to understand.


## Getting started:
### Prereq:
- Intall [Taskfile](https://taskfile.dev/installation/). Confirm installation by running `task --version`.
- Install [Go](https://go.dev/doc/install): version 1.19
- Setup local env for [Angular](https://angular.io/guide/setup-local)

### Steps
1. Clone the project. 
2. From project root (`./concurrency-vis/`), run `task init`. 
    - This installs dependencies for Angular and Go, as well as start up envoy in a docker container.
3. From project root, run `task go-serve`
4. Open a new terminal window. From project root, run `task ng-serve`
