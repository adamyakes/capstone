module gitlab.com/kshuta/concurrency-vis

go 1.19

require (
	github.com/golang/protobuf v1.3.3
	github.com/lib/pq v1.4.0
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
	google.golang.org/grpc v1.27.1
)

require (
	golang.org/x/net v0.0.0-20190311183353-d8887717615a // indirect
	golang.org/x/sys v0.0.0-20190215142949-d0b11bdaac8a // indirect
	golang.org/x/text v0.3.0 // indirect
	google.golang.org/genproto v0.0.0-20190819201941-24fa4b261c55 // indirect
)

replace gitlab.com/kshuta/concurrency-vis => ./
