package philosophers

import (
	"context"
	"fmt"
	"sync"
	"time"

	"gitlab.com/kshuta/concurrency-vis/css"
	"golang.org/x/sync/semaphore"
)

type Result struct {
	pickups, putdowns []int
	total             int
	average           float64
}

func left(id int) int            { return id }
func right(id int, Size int) int { return (id + 1) % Size }

func NonSolution(o css.Options) <-chan css.State {
	forks := make([]sync.Mutex, o.Size)
	locked := make([]bool, o.Size)
	deadlocked := new(bool)
	// This may deadlock, so we have to free resources
	go func() {
		<-time.After(time.Duration(o.Duration) + time.Second)
		*deadlocked = true
		for i := 0; i < int(o.Size); i++ {
			if locked[i] {
				forks[i].Unlock()
			}
		}
	}()

	return runSolution(
		o,
		func(id int, c chan css.State) {
			c <- css.State{Id: int32(id), State: fmt.Sprintf("Requesting fork %v", right(id, int(o.Size))), Time: time.Now().UnixNano()}
			forks[right(id, int(o.Size))].Lock()
			if *deadlocked {
				return
			}
			locked[right(id, int(o.Size))] = true
			c <- css.State{Id: int32(id), State: fmt.Sprintf("Holding fork %v", right(id, int(o.Size))), Time: time.Now().UnixNano()}

			c <- css.State{Id: int32(id), State: fmt.Sprintf("Requesting fork %v", left(id)), Time: time.Now().UnixNano()}
			forks[left(id)].Lock()
			if *deadlocked {
				return
			}
			c <- css.State{Id: int32(id), State: fmt.Sprintf("Holding fork %v", left(id)), Time: time.Now().UnixNano()}
			locked[left(id)] = true
		},
		func(id int, c chan css.State) {
			locked[right(id, int(o.Size))] = false
			c <- css.State{Id: int32(id), State: fmt.Sprintf("Releasing fork %v", right(id, int(o.Size))), Time: time.Now().UnixNano()}
			forks[right(id, int(o.Size))].Unlock()
			locked[left(id)] = false
			c <- css.State{Id: int32(id), State: fmt.Sprintf("Releasing fork %v", left(id)), Time: time.Now().UnixNano()}
			forks[left(id)].Unlock()
		},
		deadlocked,
	)
}

func Solution1(o css.Options) <-chan css.State {
	forks := make([]sync.Mutex, o.Size)
	footman := semaphore.NewWeighted(int64(o.Size - 1))
	ctx := context.Background()
	return runSolution(
		o,
		func(id int, c chan css.State) {
			if err := footman.Acquire(ctx, 1); err == nil {
				c <- css.State{Id: int32(id), State: fmt.Sprintf("Requesting fork %v", right(id, int(o.Size))), Time: time.Now().UnixNano()}
				forks[right(id, int(o.Size))].Lock()
				c <- css.State{Id: int32(id), State: fmt.Sprintf("Holding fork %v", right(id, int(o.Size))), Time: time.Now().UnixNano()}

				c <- css.State{Id: int32(id), State: fmt.Sprintf("Requesting fork %v", left(id)), Time: time.Now().UnixNano()}
				forks[left(id)].Lock()
				c <- css.State{Id: int32(id), State: fmt.Sprintf("Holding fork %v", left(id)), Time: time.Now().UnixNano()}
			} else {
				panic("catastrophic failure")
			}
		},
		func(id int, c chan css.State) {
			c <- css.State{Id: int32(id), State: fmt.Sprintf("Releasing fork %v", right(id, int(o.Size))), Time: time.Now().UnixNano()}
			forks[right(id, int(o.Size))].Unlock()
			c <- css.State{Id: int32(id), State: fmt.Sprintf("Releasing fork %v", left(id)), Time: time.Now().UnixNano()}
			forks[left(id)].Unlock()
			footman.Release(1)
		},
		new(bool),
	)
}

func Solution2(o css.Options) <-chan css.State {
	if o.Lefties == nil {
		o.Lefties = make([]bool, o.Size)
		o.Lefties[0] = true
	}
	forks := make([]sync.Mutex, o.Size)
	return runSolution(
		o,
		func(id int, c chan css.State) {
			if o.Lefties[id] {
				forks[left(id)].Lock()
				forks[right(id, int(o.Size))].Lock()
			} else {
				forks[right(id, int(o.Size))].Lock()
				forks[left(id)].Lock()
			}
		},
		func(id int, c chan css.State) {
			if o.Lefties[id] {
				forks[left(id)].Unlock()
				forks[right(id, int(o.Size))].Unlock()
			} else {
				forks[right(id, int(o.Size))].Unlock()
				forks[left(id)].Unlock()
			}
		},
		new(bool),
	)
}

func runSolution(o css.Options, getForks, putForks func(int, chan css.State), deadlocked *bool) <-chan css.State {
	c, done := make(chan css.State, 16384), make(chan bool)
	for i := 0; i < int(o.Size); i++ {
		go func(id int) {
			timeout := time.After(time.Duration(o.Duration))
			for {
				select {
				case <-timeout:
					c <- css.State{Id: int32(id), State: "Gracefully exited", Time: time.Now().UnixNano()}
					done <- true
					return
				default:
					if o.Sleep != 0 {
						time.Sleep(time.Duration(o.Sleep))
					}
					c <- css.State{Id: int32(id), State: "Hungry", Time: time.Now().UnixNano()}
					getForks(id, c)
					if *deadlocked {
						c <- css.State{Id: int32(id), State: "Deadlock detected", Time: time.Now().UnixNano()}
						done <- true
						return
					}
					c <- css.State{Id: int32(id), State: "Eating", Time: time.Now().UnixNano()}
					if o.Sleep != 0 {
						time.Sleep(time.Duration(o.Sleep))
					}
					c <- css.State{Id: int32(id), State: "Thinking", Time: time.Now().UnixNano()}
					putForks(id, c)
				}
			}
		}(i)
	}
	go func() {
		for i := 0; i < int(o.Size); i++ {
			<-done
		}
		close(c)
	}()
	return c
}
