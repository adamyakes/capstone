package main

import (
	"context"
	"database/sql"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"runtime"
	"strings"
	"time"

	_ "github.com/lib/pq"
	"gitlab.com/kshuta/concurrency-vis/css"
	"gitlab.com/kshuta/concurrency-vis/philosophers"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var db *sql.DB

var port = flag.String("port", "9090", "Port number to listen for connections.")

func main() {
	flag.Parse()

	var err error
	db, err = sql.Open("postgres", "user=postgres password=vmaSwSPwpfr76S host=localhost port=5432 sslmode=disable dbname=css")
	if err != nil {
		panic(err.Error())
	}
	lis, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%s", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer()
	reflection.Register(grpcServer)
	css.RegisterPhilosopherServer(grpcServer, &philosopherServer{})

	log.Printf("starting server on port: %s\n", *port)
	grpcServer.Serve(lis)
}

type philosopherServer struct{}

func (p *philosopherServer) Philosophize(o *css.Options, s css.Philosopher_PhilosophizeServer) error {
	log.Println("starting Philosophize")
	var c <-chan css.State
	switch o.Choice {
	case 0:
		c = philosophers.NonSolution(*o)
	case 1:
		c = philosophers.Solution1(*o)
	case 2:
		c = philosophers.Solution2(*o)
	default:
		return errors.New("bad solution choice")
	}
	fmt.Println(runtime.NumGoroutine())
	stats := make([]struct {
		stat PhilosopherStat
		last int64
	}, o.Size+1)
	for _, s := range stats {
		s.last = time.Now().UnixNano()
	}
	for v := range c {
		tmp := v
		record(stats, v)
		if err := s.Send(&tmp); err != nil {
			return err
		}
	}
	// store(*o, stats)
	return nil
}

func (p *philosopherServer) GetStats(_ context.Context, req *css.StatsReq) (*css.Runs, error) {
	var runs []*css.Run
	res, err := db.Query(`select * from philosopher_run join philosopher_stat on philosopher_run.id = philosopher_stat.philosopher_run_id order by timestamp, philosopher_number`)
	if err != nil {
		return nil, err
	}
	var lastRun css.Run
	for res.Next() {
		var throwaway int64
		var r css.Run
		var s css.Stat
		err := res.Scan(
			&r.Id,
			&r.Timestamp,
			&r.Solution,
			&r.NumPhilosophers,
			&r.SleepTime,
			&r.RunTime,
			&r.Deadlocked,
			&throwaway,
			&s.PhilosopherNumber,
			&s.MaxHungry,
			&s.MaxEating,
			&s.MaxThinking,
			&s.MinHungry,
			&s.MinEating,
			&s.MinThinking,
			&s.TotalHungry,
			&s.TotalEating,
			&s.TotalThinking,
			&s.Cycles,
			&s.AvgHungry,
			&s.AvgEating,
			&s.AvgThinking,
		)
		if err != nil {
			return nil, err
		}
		if r.Id != lastRun.Id {
			tmp := lastRun
			runs = append(runs, &tmp)
			lastRun = r
		}
		lastRun.Stats = append(lastRun.Stats, &s)
	}
	return &css.Runs{Runs: runs[1:]}, nil
}

type PhilosopherStat struct {
	MaxHungry, MaxEating, MaxThinking       int64
	MinHungry, MinEating, MinThinking       int64
	AvgHungry, AvgEating, AvgThinking       int64
	TotalHungry, TotalEating, TotalThinking int64
	Cycles                                  int
	Deadlocked                              bool
}

func record(stats []struct {
	stat PhilosopherStat
	last int64
}, s css.State) {
	stat := &stats[s.Id+1].stat
	last := &stats[s.Id+1].last
	delta := s.Time - *last
	if strings.Contains(s.State, "Hungry") {
		stat.Cycles++
		stat.TotalThinking += delta
		*last = s.Time
	} else if strings.Contains(s.State, "Eating") {
		stat.TotalHungry += delta
		*last = s.Time
	} else if strings.Contains(s.State, "Thinking") {
		stat.TotalEating += delta
		*last = s.Time
	} else if strings.Contains(s.State, "Deadlock") {
		stat.Deadlocked = true
	}
}

func store(o css.Options, stats []struct {
	stat PhilosopherStat
	last int64
}) {
	var solution string
	switch o.Choice {
	case 0:
		solution = "Non Solution"
	case 1:
		solution = "Solution 1"
	case 2:
		solution = "Solution 2"
	}
	tx, _ := db.Begin()
	res := db.QueryRow(
		"insert into philosopher_run (timestamp, solution, num_philosophers, sleep_time, runtime, deadlocked) values ($1, $2, $3, $4, $5, $6) returning id",
		time.Now(),
		solution,
		o.Size,
		o.Sleep,
		o.Duration,
		stats[1].stat.Deadlocked,
	)
	var id int
	err := res.Scan(&id)
	if err != nil {
		panic(err.Error())
	}
	for i, v := range stats {
		if i == 0 {
			continue
		}
		stat := v.stat
		_, err := db.Exec("insert into philosopher_stat (philosopher_run_id, philosopher_number, max_hungry, max_eating, max_thinking, min_hungry, min_eating, min_thinking, total_hungry, total_eating, total_thinking, cycles) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)",
			id,
			i,
			stat.MaxHungry,
			stat.MaxEating,
			stat.MaxThinking,
			stat.MinHungry,
			stat.MinEating,
			stat.MinThinking,
			stat.TotalHungry,
			stat.TotalEating,
			stat.TotalThinking,
			stat.Cycles,
		)
		if err != nil {
			panic(err.Error())
		}
	}

	err = tx.Commit()
	if err != nil {
		panic(err.Error())
	}
}
